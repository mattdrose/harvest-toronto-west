// Run lazysize setting before requiring it
window.lazySizesConfig = window.lazySizesConfig || {};
window.lazySizesConfig.lazyClass = 'js-lazy';

var G = require('gemini-loader');
var lazy = require('lazysizes');

// Slide out menu button
(function(menu) {

  menu.$button.click(function(e) {
    e.preventDefault();
    menu.$button.toggleClass('is-active');
    menu.$pageWrap.toggleClass('is-active');
  });

})({
  $button: G('#js-menu-button'),
  $menu: G('#js-full-menu'),
  $pageWrap: G('#js-wrap')
});
