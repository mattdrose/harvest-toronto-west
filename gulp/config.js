var src   = "./src";
var views = "./views";
var dest  = "./dist";

module.exports = {
  browserSync: {
    proxy: "local.wordpress.dev"
  },
  compass: {
    src: src + "/sass/**/*.scss",
    dest: dest + "/css/temp",
    settings: {
      css: dest + "/css",
      sass: src + "/sass",
      image: src + "/images"
    }
  },
  twig: {
    src: views + "/**/*.twig"
  },
  images: {
    src: src + "/images/**",
    dest: dest + "/images"
  },
  qunit: {
    options: {},
    src: "./tests/",
    dest: "./tests/bundles",
    bundles: [
      {
        src: "loading/loading.test.html",
        tests: "loading/loading.test.js"
      },
      {
        tests: "locationfinder/locationfinder.edge.js"
      }
    ]
  },
  iconFonts: {
    name: "Harvest Icon Font",
    src: require("./iconConfig"),
    dest: dest + "/fonts/icons",
    sassDest: src + "/sass",
    template: "./gulp/tasks/iconFont/template.scss.swig",
    sassOutputName: "_icons.scss",
    fontPath: "../fonts/icons",
    className: "icon",
    options: {
      fontName: "icons",
      normalize: true
    }
  },
  fonts: {
    src: src + "/fonts/**/*",
    dest: dest + "/fonts"
  },
  browserify: {
    // A separate bundle will be generated for each
    // bundle config in the list below
    bundleConfigs: [
      {
        entries: src + "/js/main.js",
        dest: dest + "/js",
        outputName: "main.js",
        // list of modules to make require-able externally
        require: ["gemini-loader"]
      },
      {
        entries: src + "/js/pages/index.js",
        dest: dest + "/js/pages",
        outputName: "index.js",
        external: ["gemini-loader"]
      }
    ]
  },
  favicons: {
    src: src + "/images/logo/favicon.png",
    dest: dest + "/images/logo/favicons",
    settings: {
      appName: "Harvest Toronto West",
      background: "#009900",
      path: dest + "/images/logo/favicons",
      html: [
        views + "/includes/favicons.twig"
      ]
    }
  },
  templates: {
    bundles: [
      /*{
        src: src + "/js/modules/alert/templates/*.hbs",
        dest: dest + "/js/modules/alert/"
      }*/
    ],
    concat: 'templates.js',
    wrap: 'Handlebars.template(<%= contents %>)',
    options: {
      root: 'exports',
      noRedeclare: true
    }
  },
  build: {
    cssSrc: dest + "/css/*.css",
    jsSrc: dest + "/js/**/*.js",
    cssDest: dest + "/css",
    jsDest: dest + "/js",
  }
};
