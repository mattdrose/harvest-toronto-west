var src = './node_modules/gemini-icons';

module.exports = [
  src + '/iconic/check.svg',
  src + '/icomoon/search.svg',
  src + '/icomoon/user.svg',
  src + '/icomoon/clock2.svg'
];
