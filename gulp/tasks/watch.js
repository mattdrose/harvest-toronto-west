/* Notes:
   - gulp/tasks/browserify.js handles js recompiling with watchify
   - gulp/tasks/browserSync.js watches and reloads compiled files
*/

var gulp     = require('gulp');
var config   = require('../config');

gulp.task('watch', [/*'templates', */'watchify', 'browserSync'], function() {
  gulp.watch(config.compass.src, ['compass']);
  gulp.watch(config.images.src,  ['images']);
  gulp.watch(config.twig.src,    ['twig']);
  // Watchify will watch and recompile our JS, so no need to gulp.watch it
});
