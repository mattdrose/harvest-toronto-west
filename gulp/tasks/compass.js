var gulp         = require('gulp');
var browserSync  = require('browser-sync');
var compass      = require('gulp-compass');
var sourcemaps   = require('gulp-sourcemaps');
var handleErrors = require('../util/handleErrors');
var config       = require('../config').compass;

gulp.task('compass', function () {
  return gulp.src(config.src)
    .pipe(sourcemaps.init())
    .pipe(compass(config.settings))
    .on('error', handleErrors)
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.dest))
    .pipe(browserSync.reload({stream:true}));
});
