var gulp        = require('gulp');
var handlebars  = require('gulp-handlebars');
var wrap        = require('gulp-wrap');
var declare     = require('gulp-declare');
var concat      = require('gulp-concat');
var ms          = require('merge-stream');
var config      = require('../config').templates;

gulp.task('templates', function() {

  var r = ms.apply(gulp, config.bundles.map(function(bundleConfig) {

    return gulp.src(bundleConfig.src)
      .pipe(handlebars())
      .pipe(wrap(config.wrap))
      .pipe(declare(config.options))
      .pipe(concat(config.concat))
      .pipe(wrap('var Handlebars = require("handlebars");\n <%= contents %>'))
      .pipe(gulp.dest(bundleConfig.dest));

  }));

  return r;
});
