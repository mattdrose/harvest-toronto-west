var gulp         = require('gulp');
var browserSync  = require('browser-sync');
var config       = require('../config').twig;

gulp.task('twig', function () {
  return gulp.src(config.src)
    .pipe(browserSync.reload({stream:true}));
});
