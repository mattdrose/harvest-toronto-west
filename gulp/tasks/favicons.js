var gulp         = require('gulp');
var handleErrors = require('../util/handleErrors');
var favicons     = require('gulp-favicons');
var config       = require('../config').favicons;

gulp.task('favicons', function() {
  return gulp.src(config.src)
    .pipe(favicons(config.settings))
    .on('error', handleErrors)
    .pipe(gulp.dest(config.dest));
});
